This is a simple python script for helping to clear DCA logs on ProfitTrailer v2.

This script is based on @grantcause "How can you dig yourself out of deep DCA logs?" given in the ProfitTrailer discord -https://discord.gg/K9a37Vh

**DISCLAIMER:** 
Run this script at your own risk. We will ask questions at each step but this is not financial advice. If you don't know the answer to any of the questions, then you probably shouldn't be running the script!!!

DISCLAIMER: 
Run this script at your own risk. We will ask questions at each step but this is not financial advice. If you don't know the answer to any of the questions, then you probably shouldn't be running the script!!!

ABOUT:
If you currently have a large DCA log you have the option of digging yourself out of those positions if you want to continue to trade (at a loss).

You have three options:
a) Wait it out and hope the market will rise and your DCA log clears itself;
b) Add a lot more funds to your trading balance to do further DCA buys to reduce the average cost of your pairs until they are in profit and clear;
c) Dig yourself out of your DCA log positions;

This script focuses on option c.

OPTION C:

Step 1: Start by putting ProfitTrailer into Sell Only Mode. Enter the following into the browser adjusting the IP Address and Port Number to suit your setup:

https://localhost:8081/settings/sellOnlyMode?type=&enabled=true

The SOM indicator should now show red in the top right of the ProfitTrailer screen if you are running PT v1x or in the bottom right of the screen if you are running PT 2.x(edited)

Step 2: 

Clone or download this repo somewhere on your computer/server:

```
git clone git@gitlab.com:TradeFlu/PT-DCA-Digger.git
```
OR
```
wget -O - https://gitlab.com/TradeFlu/PT-DCA-Digger/-/archive/master/PT-DCA-Digger-master.zip | unzip -
```

Then
```
cd PT-DCA-Digger
```

Step 3:

I like pipenv myself but you can use whatever method you want to install requirements.txt. As always, run in a virtual environment where possible.

```
pipenv install
```
OR
```
pip install -r requirements
```

Step 4:

Run the script and answer the prompts!
```
python dig.py
```

If you like this script and it heps dig you out of your hole, please feel free to make a small donation.

BTC: 3HXbjkf9fUT1rKr9ZEGegQ5tmvHefuVqt8

<img src="/img/Bitcoin_QR_code.png" width="200" height="200">

ETH: 0xBD2161F69e4f80f1818F62c073002BbAC915c910

<img src="/img/Ethereum_QR_code.png" width="200" height="200">

LTC: LToQit5H4LwT2FknePuP61G4i4FGwgZsBX

<img src="/img/Litecoin_QR_code.png" width="200" height="200">

BCH: qzc30rqgd6lnmk35q5mhs0f09h3y6st6kvs869rk2a

<img src="/img/BitcoinCash_QR_code.png" width="200" height="200">